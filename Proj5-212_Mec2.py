# -*- coding: utf-8 -*-
# Resolve problema 5-212 do livro do Meriam e Kraige, 7a. edicao

import math
from matplotlib import pyplot

r=float(input("digite o comprimento r em mm:"))
l=float(input("digite o comprimento l em mm:"))
w=float(input("digite a velocidade angular em rpm :"))

Wrads=(w*2*math.pi)/60.
rm=r/1000.0
lm=l/1000.0

teta= range(0,180,1)

Va=[Wrads*rm*math.sin(i*math.pi/180)+(rm**2*Wrads*math.cos(i*math.pi/180)*math.sin(i*math.pi/180))/(math.sqrt(rm**2*math.cos(i*math.pi/180)-rm**2+l**2)) for i in teta]

pyplot.plot(teta,Va)

pyplot.grid(True)

pyplot.xlabel(r'$\theta\,(^{o})$')
pyplot.xlim([0,180])
pyplot.ylabel("Velocidade (m/s)")

pyplot.title("Projeto 5-212")

pyplot.savefig('graficop5-212.png')    

pyplot.show()

# Encontrar numericamente a raiz da derivada de va (o ponto de máximo)

def dva_dteta(teta):
    return -Wrads*rm*math.cos(teta)-rm**4*Wrads*math.sin(teta)/(4*math.sqrt(rm**2*math.cos(teta)-rm**2+lm**2))

from scipy.optimize import fsolve

tetamax = fsolve(dva_dteta,80*math.pi/180)[0]

print(f'O angulo com velocidade maxima (em rad) é {tetamax*180/math.pi:.2f} graus.')

Va=Wrads*rm*math.sin(tetamax)+(rm**2*Wrads*math.cos(tetamax)*math.sin(tetamax))/(math.sqrt(rm**2*math.cos(tetamax)-rm**2+l**2))

print(f'A velocidade maxima (em m/s) é {Va:.2f} m/s')